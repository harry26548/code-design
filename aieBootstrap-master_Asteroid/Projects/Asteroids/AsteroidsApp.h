#pragma once
#include "Application.h"
#include "Renderer2D.h"
#include <vector>

#include "GameStateManager.h"
#include "MenuState.h"
#include "GameState.h"
#include "InGameState.h"
#include "PauseState.h"

using std::vector;


class AsteroidsApp : public aie::Application {
public:
	AsteroidsApp();
	virtual ~AsteroidsApp() = default;

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:
	bool				mGameIsOver;
	float				mTimeInterval;
	double 				mTimer;
	int					mScore;
	int					m_foo;
	int*				m_pfoo;
	aie::Texture*		m_texture;
	aie::Texture*		m_playerTexture;
	aie::Renderer2D*	m_2dRenderer;
	aie::Font*			m_font;

	GameStateManager* mcManager;
	InGameState* a_gameState;
	MenuState* a_menuState;
	PauseState* a_pauseState;
};