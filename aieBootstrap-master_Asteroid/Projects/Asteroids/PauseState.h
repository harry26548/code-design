#pragma once
#include "GameState.h"
#include "AsteroidsApp.h"
#include "InGameState.h"

class AsteroidsApp;
class InGameState;


class PauseState : public GameState {


public:
	PauseState(AsteroidsApp* app, InGameState* gameApp);
	~PauseState();

	virtual void onUpdate(float deltaTime, double m_timer);
	virtual void onDraw(aie::Renderer2D* m_2drenderer);
	virtual void onExit();
	virtual void onEnter(double m_timer);

private:

	AsteroidsApp* app;
	InGameState* gameApp;
};

