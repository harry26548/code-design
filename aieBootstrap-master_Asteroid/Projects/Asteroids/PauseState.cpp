#include "PauseState.h"
#include "Input.h"


//object creation
PauseState::PauseState(AsteroidsApp* app, InGameState* gameApp) : app(app), gameApp(gameApp) {}

//destructor
PauseState::~PauseState() {}

//update
void PauseState::onUpdate(float deltaTime, double m_timer) {

	//get input instance
	aie::Input* input = aie::Input::getInstance();

	//if q is pressed, exit the game.
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE)) {

		app->quit();
	}
}

//draw 
void PauseState::onDraw(aie::Renderer2D * m_2drenderer) {

}

//delete everything
void PauseState::onExit() {

}

//this function is called whenever this state is opened/activated, not created. 
void PauseState::onEnter(double m_timer) {


}
