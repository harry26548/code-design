#pragma once
#include <vector>
#include <list>
#include "GameState.h"
#include "Renderer2D.h"



class GameStateManager {


public:
	//get instance, as this is a singleton class
	static GameStateManager* getInstance();

	void registerState(int id, GameState* state);

	void pushState(int id);

	void popState();

	void update(float deltaTime, double m_timer);

	void draw(aie::Renderer2D* m_2drenderer);

	unsigned int activeStateCount() const;

	GameState* getTopState();

	GameState* getState(int id);



private:
	//constructor/destructor private because singleton class
	GameStateManager();
	virtual ~GameStateManager();

	static GameStateManager* m_GSinstance;

	std::list<GameState*> m_pushedStates;
	bool m_popState = false;

	std::list<GameState*> m_stateStack;
	std::vector<GameState*> m_registeredStates;


};


