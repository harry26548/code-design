#pragma once
#include "GameState.h"
#include "AsteroidsApp.h"

class AsteroidsApp;

class MenuState : public GameState {
public:
	MenuState(AsteroidsApp* app);
	~MenuState();

public:

	virtual void onUpdate(float deltaTime, double m_timer);
	virtual void onDraw(aie::Renderer2D* m_2drenderer);
	virtual void onExit();
	virtual void onEnter(double m_timer);

private:

	AsteroidsApp* app;
};