#include "MenuState.h"


MenuState::MenuState(AsteroidsApp* app) : app(app) {}


MenuState::~MenuState() {}

//update
void MenuState::onUpdate(float deltaTime, double m_timer) {}

//draw
void MenuState::onDraw(aie::Renderer2D * m_2drenderer) {


}

//delete everything
void MenuState::onExit() {}


//initalise everything that needs to be initialised on entrance
void MenuState::onEnter(double m_timer) {

}
