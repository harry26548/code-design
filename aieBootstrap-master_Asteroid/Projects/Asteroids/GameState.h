#pragma once
#include "Renderer2D.h"
#include <iostream>
class GameState {
	friend class GameStateManager;
public:
	GameState() {};
	virtual ~GameState() {};
	bool isActive() const { return m_active; }
	void unpause() { m_active = true; }


protected:

	//pretty much entirely virtual class, all states are derived from this

	virtual void onEnter(double m_timer) {}

	virtual void onUpdate(float deltaTime, double m_timer) = 0;
	virtual void onDraw(aie::Renderer2D* m_2drenderer) = 0;
	void enter(double m_timer) {
		m_active = true;
		onEnter(m_timer);
	}
	void exit() { m_active = false; onExit(); }
	void pause() { m_active = false; }

	//can be overwritten in derived classes that need to behave in certain ways when activated/deactivated or pushed/popped.

	virtual void onExit() {}
	virtual void onPushed() {}
	virtual void onPopped() {}

private:

	bool m_active = false;

};