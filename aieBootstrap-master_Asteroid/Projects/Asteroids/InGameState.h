#pragma once
#include "GameState.h"
#include "AsteroidsApp.h"

class AsteroidsApp;

class InGameState : public GameState {

public:
	InGameState(const AsteroidsApp* app);
	~InGameState();

	virtual void onUpdate(float deltaTime, double m_timer);
	virtual void onDraw(aie::Renderer2D* m_2drenderer);
	virtual void onExit();
	virtual void onEnter(double m_timer);

private:

	const AsteroidsApp* app;

};
