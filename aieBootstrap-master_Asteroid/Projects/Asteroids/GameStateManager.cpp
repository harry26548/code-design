#include "GameStateManager.h"
#include <iostream>


GameStateManager::GameStateManager() {

}

//return a static instance of the gamestatemanager, as this is singleton class.
GameStateManager * GameStateManager::getInstance() {


	static GameStateManager* m_GSinstance = new GameStateManager();
	return m_GSinstance;
}

//call clear, which deletes everything in all state lists.
GameStateManager::~GameStateManager() {

	m_registeredStates.clear();
	m_pushedStates.clear();
	m_stateStack.clear();
}

//register a state in the registered state list.
void GameStateManager::registerState(int id, GameState * state) {

	m_registeredStates[id] = state;
}

//add a state to the end of the the pushed states.
void GameStateManager::pushState(int id) {

	m_pushedStates.push_back(m_registeredStates[id]);
}

//set a state's pop state to be true, which will remove it in the next update
void GameStateManager::popState() {

	m_popState = true;
}

//update
void GameStateManager::update(float deltaTime, double m_timer) {

	//if pop state is true, then remove current top, and if the statestack is not empty, enter the next one.
	while (m_popState) {
		m_popState = false;

		//deactivate previous top
		m_stateStack.back()->exit();
		auto temp = m_stateStack.back();
		m_stateStack.pop_back();
		temp->onPopped();

		//activates the one under the previous top if exists
		if (m_stateStack.empty() == false) {

			m_stateStack.back()->enter(m_timer);
		}
	}

	//push any states that were pushed since last update
	for (auto pushedState : m_pushedStates) {

		//deactivate the previous top
		if (m_stateStack.empty() == false) {
			if (m_stateStack.back()->isActive()) {
				m_stateStack.back()->exit();
			}
		}

		//activate new top
		pushedState->onPushed();
		m_stateStack.push_back(pushedState);
		m_stateStack.back()->enter(m_timer);
	}
	m_pushedStates.clear();

	//call update on all active states.
	for (auto state : m_stateStack) {
		if (state->isActive()) {
			state->onUpdate(deltaTime, m_timer);
		}
	}

}

//draw
void GameStateManager::draw(aie::Renderer2D* m_2drenderer) {

	//draw all states
	for (auto state : m_stateStack) {

		state->onDraw(m_2drenderer);
	}
}

//return amount of active states
unsigned int GameStateManager::activeStateCount() const {

	return m_stateStack.size();
}

//return the top state
GameState* GameStateManager::getTopState() {

	return m_stateStack.back();
}

//get state at index
GameState * GameStateManager::getState(int id) {

	return m_registeredStates[id];
}
