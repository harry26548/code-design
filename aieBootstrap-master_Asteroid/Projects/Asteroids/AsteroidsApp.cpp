#include "AsteroidsApp.h"
#include "Texture.h"
#include "Font.h"
#include "Input.h"
#include <string>
#include <sstream>
#include <algorithm>
#include <iostream>

// constructor
AsteroidsApp::AsteroidsApp() {}

// on startup will be accessed once									  	   
bool AsteroidsApp::startup() {											   
	m_2dRenderer = new aie::Renderer2D();								   

	mcManager = GameStateManager::getInstance();
	a_gameState = new InGameState(this);
	a_menuState = new MenuState(this);
	a_pauseState = new PauseState(this, a_gameState);
	mcManager->registerState(1, a_menuState);
	mcManager->registerState(2, a_gameState);
	mcManager->registerState(3, a_pauseState);
	mcManager->pushState(0);
	// set up ball
	mTimer = 0;															   
	return true;														   
}																		   
																		   
// on shutdown will be accessed once								  	   
void AsteroidsApp::shutdown() {											   
	//delete m_texture;												  	   
	//delete m_playerTexture;										  	   													   
	delete m_2dRenderer;												   
}

// called at 60 fps (approx)
void AsteroidsApp::update(float deltaTime) {
	mTimer += deltaTime;

	//call the GameStateManagers updates, which updates all registered pushed states.
	mcManager->update(deltaTime, mTimer);
}

// draw the screen
void AsteroidsApp::draw() {
	// wipe the screen to the background colour
	clearScreen();
	// begin drawing sprites
	m_2dRenderer->begin();

	//draw the gamestatemanagers registered pushed states
	mcManager->draw(m_2dRenderer);

	m_2dRenderer->end();
}