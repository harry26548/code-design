#include "AsteroidsApp.h"
#include <time.h>

int main() {
	srand((unsigned int)time(NULL));
	auto app = new AsteroidsApp();
	app->run("AIE", 1280, 720, false);
	delete app;

	return 0;
}