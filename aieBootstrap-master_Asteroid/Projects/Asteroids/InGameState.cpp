#include "InGameState.h"
#include "GameStateManager.h"

//object creation
InGameState::InGameState(const AsteroidsApp* app) : app(app) {

}

InGameState::~InGameState() {

}

//update
void InGameState::onUpdate(float deltaTime, double m_timer) {

}

//draw
void InGameState::onDraw(aie::Renderer2D * m_2drenderer) {

}

//delete everything 
void InGameState::onExit() {

}

//this function is called whenever this state is opened/activated, not created. 
void InGameState::onEnter(double m_timer) {

}