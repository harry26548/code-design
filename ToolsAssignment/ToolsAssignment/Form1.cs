﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;

namespace ToolsAssignment {
	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent();
			Bitmap bmp = new Bitmap(Canvas.Width, Canvas.Height);

			for (int x = 0; x < Canvas.Width; x++) {
				for (int y = 0; y < Canvas.Height; y++) {
					bmp.SetPixel(x, y, Color.White);
				}
			}


			Canvas.Image = bmp;
		}

		Graphics g;

		string currentBrush = "solidSquare";

		bool isMouseDown = false;

		private void Canvas_MouseDown(object sender, MouseEventArgs e) {
			isMouseDown = true;
		}

		private void Canvas_MouseMove(object sender, MouseEventArgs e) {
			if (isMouseDown) {
				using (Graphics g = Graphics.FromImage(Canvas.Image)) {
					g.SmoothingMode = SmoothingMode.AntiAlias;
					if (currentBrush == "solidSquare") {
						Rectangle drawSqua = new Rectangle(e.X - ((int)float.Parse(LineWidth.Text) / 2), e.Y - ((int)float.Parse(LineWidth.Text) / 2), (int)float.Parse(LineWidth.Text), (int)float.Parse(LineWidth.Text));
						Brush solidBrush = new SolidBrush(SelectedColour.BackColor);

						g.FillRectangle(solidBrush, drawSqua);

						solidBrush.Dispose();
					} else if (currentBrush == "solidCircle") {
						Rectangle drawCircle = new Rectangle(e.X - ((int)float.Parse(LineWidth.Text) / 2), e.Y - ((int)float.Parse(LineWidth.Text) / 2), (int)float.Parse(LineWidth.Text), (int)float.Parse(LineWidth.Text));
						Brush solidBrush = new SolidBrush(SelectedColour.BackColor);
						g.FillEllipse(solidBrush, drawCircle);
						solidBrush.Dispose();
					}
				}

				Canvas.Invalidate();
			}
		}

		private void Canvas_MouseUp(object sender, MouseEventArgs e) {
			isMouseDown = false;
		}

		private void SelectedColour_Click(object sender, EventArgs e) {
			ColorDialog c = new ColorDialog();
			if (c.ShowDialog() == DialogResult.OK) {
				SelectedColour.BackColor = c.Color;
			}
		}

		private void CanvasColour_Click(object sender, EventArgs e) {
			ColorDialog c = new ColorDialog();
			if (c.ShowDialog() == DialogResult.OK) {
				Canvas.BackColor = c.Color;
				CanvasColour.BackColor = c.Color;
			}
		}

		private void ClearButton_Click(object sender, EventArgs e) {
            using (Graphics g = Graphics.FromImage(Canvas.Image))
            {
                g.Clear(Canvas.BackColor);
                Canvas.BackColor = Color.White;
            }
		}

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e) {
			SaveFileDialog saveFile = new SaveFileDialog(); 
			saveFile.Filter = "Image Files(*.BMP;*.JPG;*.PNG)|*.BMP;*.JPG;*.PNG|All files (*.*)|*.*";
			saveFile.Title = "Save current Canvas";
			saveFile.ShowDialog();
			if (saveFile.FileName != "") {
				if (saveFile.FileName.Contains(".bmp") || saveFile.FileName.Contains(".BMP")) {
					System.IO.FileStream fs = (FileStream)saveFile.OpenFile();
					Canvas.Image.Save(fs, ImageFormat.Bmp);
				} else if (saveFile.FileName.Contains(".jpg") || saveFile.FileName.Contains(".jpeg") || saveFile.FileName.Contains(".JPG") || saveFile.FileName.Contains(".JPEG")) {
					System.IO.FileStream fs = (FileStream)saveFile.OpenFile();
					Canvas.Image.Save(fs, ImageFormat.Jpeg);
				} else if (saveFile.FileName.Contains(".png") || saveFile.FileName.Contains(".PNG")) {
					System.IO.FileStream fs = (FileStream)saveFile.OpenFile();
					Canvas.Image.Save(fs, ImageFormat.Png);
				} else {
					MessageBox.Show("Name cannot be empty");
				}
			}
		}

		private void openToolStripMenuItem_Click(object sender, EventArgs e) {
			OpenFileDialog fd = new OpenFileDialog();

			fd.DefaultExt = "bmp";

			fd.Title = "Browse bitmap files";

			fd.Filter = "Image Files(*.bmp)|*.bmp;|All files(*.*)|*.*";

			fd.Multiselect = false;

			string file = "";

			if (fd.ShowDialog() == DialogResult.OK) {
				file = fd.FileName;
			}
			Bitmap temp = new Bitmap(file);
			Bitmap bm = new Bitmap(temp, Canvas.Width, Canvas.Height);
			Canvas.Image = bm;
		}

		private void square_Click(object sender, EventArgs e) { currentBrush = "solidSquare"; }

		private void circle_Click(object sender, EventArgs e) { currentBrush = "solidCircle"; }
	}
}