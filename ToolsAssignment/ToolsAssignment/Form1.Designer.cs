﻿namespace ToolsAssignment
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.LineWidth = new System.Windows.Forms.ComboBox();
			this.SelectedColour = new System.Windows.Forms.Button();
			this.CanvasColour = new System.Windows.Forms.Button();
			this.ClearButton = new System.Windows.Forms.Button();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.file = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.circle = new System.Windows.Forms.Button();
			this.square = new System.Windows.Forms.Button();
			this.Canvas = new System.Windows.Forms.PictureBox();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Canvas)).BeginInit();
			this.SuspendLayout();
			// 
			// LineWidth
			// 
			this.LineWidth.FormattingEnabled = true;
			this.LineWidth.Location = new System.Drawing.Point(12, 29);
			this.LineWidth.Name = "LineWidth";
			this.LineWidth.Size = new System.Drawing.Size(121, 21);
			this.LineWidth.TabIndex = 1;
			this.LineWidth.Text = "2";
			// 
			// SelectedColour
			// 
			this.SelectedColour.BackColor = System.Drawing.SystemColors.MenuHighlight;
			this.SelectedColour.Location = new System.Drawing.Point(12, 56);
			this.SelectedColour.Name = "SelectedColour";
			this.SelectedColour.Size = new System.Drawing.Size(121, 23);
			this.SelectedColour.TabIndex = 2;
			this.SelectedColour.UseVisualStyleBackColor = false;
			this.SelectedColour.Click += new System.EventHandler(this.SelectedColour_Click);
			// 
			// CanvasColour
			// 
			this.CanvasColour.Location = new System.Drawing.Point(12, 85);
			this.CanvasColour.Name = "CanvasColour";
			this.CanvasColour.Size = new System.Drawing.Size(121, 23);
			this.CanvasColour.TabIndex = 3;
			this.CanvasColour.UseVisualStyleBackColor = true;
			this.CanvasColour.Click += new System.EventHandler(this.CanvasColour_Click);
			// 
			// ClearButton
			// 
			this.ClearButton.Location = new System.Drawing.Point(12, 114);
			this.ClearButton.Name = "ClearButton";
			this.ClearButton.Size = new System.Drawing.Size(121, 23);
			this.ClearButton.TabIndex = 4;
			this.ClearButton.Text = "Clear";
			this.ClearButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
			this.ClearButton.UseVisualStyleBackColor = true;
			this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.file});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1160, 24);
			this.menuStrip1.TabIndex = 5;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// file
			// 
			this.file.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveAsToolStripMenuItem});
			this.file.Name = "file";
			this.file.Size = new System.Drawing.Size(37, 20);
			this.file.Text = "File";
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.openToolStripMenuItem.Text = "Open";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
			// 
			// saveAsToolStripMenuItem
			// 
			this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
			this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.saveAsToolStripMenuItem.Text = "Save";
			this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
			// 
			// circle
			// 
			this.circle.Image = global::ToolsAssignment.Properties.Resources.circle;
			this.circle.Location = new System.Drawing.Point(73, 143);
			this.circle.Name = "circle";
			this.circle.Size = new System.Drawing.Size(60, 60);
			this.circle.TabIndex = 7;
			this.circle.Text = "button2";
			this.circle.UseVisualStyleBackColor = true;
			this.circle.Click += new System.EventHandler(this.circle_Click);
			// 
			// square
			// 
			this.square.Image = global::ToolsAssignment.Properties.Resources.square;
			this.square.Location = new System.Drawing.Point(12, 143);
			this.square.Name = "square";
			this.square.Size = new System.Drawing.Size(60, 60);
			this.square.TabIndex = 6;
			this.square.UseVisualStyleBackColor = true;
			this.square.Click += new System.EventHandler(this.square_Click);
			// 
			// Canvas
			// 
			this.Canvas.BackColor = System.Drawing.SystemColors.Menu;
			this.Canvas.Location = new System.Drawing.Point(139, 27);
			this.Canvas.Name = "Canvas";
			this.Canvas.Size = new System.Drawing.Size(1009, 755);
			this.Canvas.TabIndex = 0;
			this.Canvas.TabStop = false;
			this.Canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseDown);
			this.Canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseMove);
			this.Canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Canvas_MouseUp);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.WindowText;
			this.ClientSize = new System.Drawing.Size(1160, 794);
			this.Controls.Add(this.circle);
			this.Controls.Add(this.square);
			this.Controls.Add(this.ClearButton);
			this.Controls.Add(this.CanvasColour);
			this.Controls.Add(this.SelectedColour);
			this.Controls.Add(this.LineWidth);
			this.Controls.Add(this.Canvas);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "Form1";
			this.Text = "PaintToolMemes";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Canvas)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Canvas;
        private System.Windows.Forms.ComboBox LineWidth;
        private System.Windows.Forms.Button SelectedColour;
        private System.Windows.Forms.Button CanvasColour;
		private System.Windows.Forms.Button ClearButton;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem file;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.Button square;
		private System.Windows.Forms.Button circle;
	}
}

