#pragma once

#include "Behaviour.h"

class Path;

class FollowPathBehaviour : public Behaviour {
public:
	FollowPathBehaviour();
	virtual ~FollowPathBehaviour();

	virtual void Update(GameObject *object, float deltaTime);
	virtual void Draw(GameObject *object, aie::Renderer2D *renderer);

	Path* GetPath();
	void SetPath(Path *path);

	float GetStrength();
	void SetStrength(float strength);

	float GetNodeRadius();
	void SetNodeRadius(float radius);

protected:
	int m_currentPathNodeIndex;

	Path *m_path;
	float m_strength;

	float m_nodeRadius;

private:
};