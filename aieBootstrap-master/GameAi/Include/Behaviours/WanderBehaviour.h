#pragma once
#include "Behaviour.h"
#include <glm\vec2.hpp>
#include "GameObject.h"

class WanderBehaviour : public Behaviour {
public:
	WanderBehaviour();
	virtual ~WanderBehaviour();

	virtual void Update(GameObject *object, float deltaTime);
	virtual void Draw(GameObject *object, aie::Renderer2D *renderer);

	void SetStrength(float strength);
	float GetStrength();

protected:
	float m_wanderAngle;
	float m_forceStrength;
	glm::vec2 m_targetPos;
	glm::vec2 m_lastPosition;
private:
};