#pragma once
#include "Behaviour.h"
#include <glm\detail\type_mat4x2.hpp>
#include <glm\detail\type_vec.hpp>
#include <functional>

class GameObject;
class SeekBehaviour;

class PursueBehaviour : public Behaviour {
public:
	PursueBehaviour();
	virtual ~PursueBehaviour();
	void Update(GameObject *object, float deltaTime);
	glm::vec2 GetFuturePosition() const;
	void SetFuturePosition(glm::vec2 val);
	GameObject *GetTarget();
	void SetTarget(GameObject * val);
	float GetForceStrength() const;
	void SetForceStrength(float val);

	void Draw(GameObject *object, aie::Renderer2D *renderer);

	const glm::vec2 &GetTargetPos();
	void SetTargetPos(const glm::vec2 &target);

	void SetOuterRadius(float radius);
	float GetOuterRadius();

	void SetInnerRadius(float radius);
	float GetInnerRadius();

	void OnInnerRadiusEnter(std::function< void() > func);
	void OnInnerRadiusExit(std::function< void() > func);
	void OnOuterRadiusEnter(std::function< void() > func);
	void OnOuterRadiusExit(std::function< void() > func);

	std::function < void() > m_onInnerRadiusEnter;
	std::function < void() > m_onInnerRadiusExit;
	std::function < void() > m_onOuterRadiusEnter;
	std::function < void() > m_onOuterRadiusExit;
protected:
	glm::vec2 m_futurePosition;
	glm::vec2 m_dirToTarget;
	GameObject *m_target;
	glm::vec2 m_targetPos;
	float m_forceStrength;
	SeekBehaviour *m_seekBehaviour;
	float m_innerRadius;
	float m_outerRadius;

private: 
	glm::vec2 m_lastPosition;
};