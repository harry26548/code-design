#pragma once

#include "Graph\Graph2D.h"
#include <functional>
#include "Path.h"
#include <list>

namespace aie {
	class Renderer2D;
}


class Pathfinder {

protected:
	struct Node {
		Graph2D::Node *node = nullptr;
		Node* parent = nullptr;
		float gScore;
	};

public:

	Pathfinder(Graph2D *graph);
	~Pathfinder();

	Graph2D *GetGraph();
	void SetGraph(Graph2D *graph);

	void FindPath(Graph2D::Node *, std::function<bool(Graph2D::Node *)> func);

	void Draw(aie::Renderer2D *renderer);

	bool PathFound();
	void UpdateSearch();
	Path& GetPath();

protected:
	Graph2D *m_graph = nullptr;
	std::function<bool(Graph2D::Node *)> m_isGoalNodeFunc;
	bool m_pathFound;
	Path m_path;
	std::list<Node* > m_open;
	std::list<Node* > m_closed;
private:

	Pathfinder::Node* GetNodeInList(std::list<Node* > &list, Graph2D::Node *node);
	void CalculatePath(Node *goal);
};