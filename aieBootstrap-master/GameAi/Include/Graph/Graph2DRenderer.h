#pragma once
#include "Graph\Graph2D.h"

class Graph2DRenderer {
public:

	Graph2DRenderer();
	~Graph2DRenderer();

	void Draw(aie::Renderer2D *renderer);

	void Update(float dt);

	void SetGraph(Graph2D *graph);
	Graph2D *GetGraph();

protected:

	Graph2D *m_graph;

private:
};
