#pragma once

#include <vector>

template<typename TNodeData, typename TEdgeData>
class Graph {
public:
	struct Edge;
	struct Node;
	struct Node {
		TNodeData data;
		std::vector<Edge> edges;
	};
	struct Edge {
		Node *to;
		TEdgeData data;
	};

public:
	Graph() {}

	virtual ~Graph() {
		for (auto iter = m_nodes.begin(); iter != m_nodes.end(); iter++) {
			delete (*iter);
		}
		m_nodes.clear();
	}

	int AddNode(const TNodeData &data) {
		Node *node = new Node();
		node->data = data;
		m_nodes.push_back(node);
		return m_nodes.size() - 1;
	}

	void AddEdge(int nodeAIndex, int nodeBIndex, bool biDirectional, const TEdgeData &data) {
		Node *nodeA = GetNodeById(nodeAIndex);
		Node *nodeB = GetNodeById(nodeBIndex);
		AddEdge(nodeA, nodeB, biDirectional, data);
	}

	void AddEdge(Node *nodeA, Node *nodeB, bool biDirectional, const TEdgeData &data) {
		Edge edge1;
		edge1.to = nodeB;
		edge1.data = data;
		nodeA->edges.push_back(edge1);

		if (biDirectional) {
			Edge edge2;
			edge2.to = nodeA;
			edge2.data = data;
			nodeB->edges.push_back(edge2);
		}
	}

	Node *GetNodeById(int nodeId) {
		return  m_nodes[nodeId];
	}

	const std::vector<Node *>& GetNodes() const {
		return m_nodes;
	}

public:
protected:
	std::vector<Node *> m_nodes;
private:
};