#pragma once

#include "Graph.hpp"
#include <glm\vec2.hpp>

namespace aie {
	class Renderer2D;
}

class Graph2D : public Graph<glm::vec2, float> {
public:
	Graph2D() : Graph() {}
	virtual ~Graph2D() {}

	void GetNearbyNodes(glm::vec2 pos, float radius, std::vector<Graph2D::Node *> &out_collection);

protected:
private:
};