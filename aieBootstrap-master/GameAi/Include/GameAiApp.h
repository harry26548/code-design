#pragma once

#include "Application.h"
#include "Renderer2D.h"
#include <glm\glm.hpp>

class GameObject;
class Graph2D;
class EntitiesManager;
class Graph2DRenderer;
class Player;
class NPC;

class GameAiApp : public aie::Application {
public:
	GameAiApp();

	virtual ~GameAiApp();
	virtual bool startup();
	virtual void shutdown();
	virtual void update(float deltaTime);
	virtual void draw();
	void SetUpGraph();
	void UpdateGraph(float deltaTime);
	void DrawGraph();

protected:
	aie::Renderer2D* m_2dRenderer;
	aie::Font* m_font;
	glm::vec2 m_mousePos;
	float m_graphToMouseDistance;
	Graph2D *m_graph;
	EntitiesManager *m_entities;
	Graph2DRenderer *m_graphRenderer;
};