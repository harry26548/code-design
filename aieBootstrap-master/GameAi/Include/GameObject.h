#pragma once
#include <glm\vec2.hpp>

class Behaviour;

namespace aie {
	class Renderer2D;
}

class GameObject {
public:
	GameObject();
	virtual ~GameObject();
	virtual void Update(float deltaTime);
	virtual void Draw(aie::Renderer2D *renderer);
	void SimulatePhysics(float deltaTime);
	void ApplyForce(const glm::vec2 &force);
	void ApplyDamage(const int &health);
	glm::vec2 & GetPosition();
	glm::vec2 & GetVelocity();
	float GetFriction();
	Behaviour* GetBehaviour();
	int GetHealth();
	bool IsAlive();
	bool IsNearby();
	void SetPosition(const glm::vec2 pos);
	void SetVelocity(const glm::vec2 vel);
	void SetFriction(float fric);
	void SetBehaviour(Behaviour *behaviour);
	void SetHealth(int health);
	void SetIsAlive(bool alive);
	void SetIsNearby(bool near);
protected:
	glm::vec2 m_position;
	glm::vec2 m_velocity;
	glm::vec2 m_acceleration;
	int m_health;
	bool m_isAlive;
	float m_friction;
	bool m_isNearby;
	Behaviour *m_behaviour;
private:
};