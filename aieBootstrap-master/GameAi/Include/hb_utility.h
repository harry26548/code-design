#pragma once

//Wander Behaviour
#define WANDER_RADIUS 200.f
#define WANDER_CIRCLE_DISTANCE 5.f
#define WANDER_ANGLE_CHANGE 3.f

//GameAIApp
#define GRAPH_NUM_ROWS 15
#define GRAPH_NUM_COLS 27%
#define GRAPH_SPACING 48
#define GRAPH_XOFFSET 25
#define GRAPH_YOFFSET 25
#define GRAPH_EDGE_MAX_LENGTH 75
#define FONT_SIZE 15

//NPC
#define OUTER_RADIUS 250

//ENTITIES
#define NPC_DAMAGE 10
#define PLAYER_DAMAGE 10

void clamp(float& val, float min, float max);