#pragma once

#include "GameObject.h"
#include "Graph\Graph2D.h"

class WanderBehaviour;
class KeyboardBehaviour;
class SeekBehaviour;
class FollowPathBehaviour;
class PursueBehaviour;
class Path;
class Graph2D;
class Pathfinder;

class Player : public GameObject {
public:
	Player();
	virtual ~Player();

	void Update(float deltaTime);

	void Draw(aie::Renderer2D *renderer);

	Graph2D * GetGraph() const;
	void SetGraph(Graph2D * graph);

protected:
	//Behaviour
	KeyboardBehaviour *m_keyboardBehaviour;
	SeekBehaviour *m_seekBehaviour;
	SeekBehaviour *m_fleeBehaviour;
	WanderBehaviour *m_wanderBehaviour;
	FollowPathBehaviour *m_followPathBehaviour;

	Path* m_path;
	Graph2D *m_graph;
	Graph2D::Node *m_startNode;
	Graph2D::Node *m_endNode;
	Pathfinder *m_pathfinder;

private:

};