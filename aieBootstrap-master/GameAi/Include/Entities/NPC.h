#pragma once
#include "GameObject.h"
#include "Graph\Graph2D.h"

class PursueBehaviour;
class SeekBehaviour;
class FollowPathBehaviour;
class Path;
class Pathfinder;
class Graph2D;

class NPC : public GameObject {
public:
	NPC(GameObject *player);
	
	virtual ~NPC();

	void Update(float deltaTime);
	void Draw(aie::Renderer2D *renderer);

	Graph2D * GetGraph() const;
	void SetGraph(Graph2D * graph);
protected:
	PursueBehaviour *m_pursueBehaviour;
	SeekBehaviour *m_fleeBehaviour;
	FollowPathBehaviour *m_followPathBehaviour;

	GameObject* m_player;

	Path* m_path;
	Graph2D *m_graph;
	Graph2D::Node *m_startNode;
	Graph2D::Node *m_endNode;
	Pathfinder *m_pathfinder;
private:
};