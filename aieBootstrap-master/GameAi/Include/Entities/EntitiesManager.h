#pragma once
#include <glm\vec2.hpp>
#include <vector>

class GameObject;
class Graph2D;

namespace aie {
	class Renderer2D;
}

class EntitiesManager {
public:
	EntitiesManager();
	virtual ~EntitiesManager();

	void CreatePlayer(glm::vec2 pos, Graph2D* graph);

	void CreateNPC(glm::vec2 pos, Graph2D* graph);

	std::vector<GameObject *>& GetObjectsVector();

	void GetNearbyEntities(GameObject* object, float radius, std::vector<GameObject *> &out_collection);

	GameObject* GetPlayer();

	void Update(float dt);

	void Draw(aie::Renderer2D* renderer);

protected:
	std::vector<GameObject *> m_gameObjects;
private:
};