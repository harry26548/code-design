#include "hb_utility.h"
#include <algorithm>

void clamp(float& val, float min, float max) {
	val = std::max(min, std::min(val, max));
}