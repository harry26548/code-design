#include "Behaviours\WanderBehaviour.h"
#include <random>
#include <ctime>
#include "GameObject.h"
#include <glm\vec2.hpp>
#include <glm\glm.hpp>
#include <math.h>
#include "hb_utility.h"

WanderBehaviour::WanderBehaviour() : m_wanderAngle(0.f), m_targetPos(500.f, 500.f) {}

WanderBehaviour::~WanderBehaviour() {}

void WanderBehaviour::Update(GameObject * object, float deltaTime) {
//#if 0
	srand((unsigned int)time(NULL));

	// If there is no velocity, set a random one.
	if (object->GetVelocity().x == 0 && object->GetVelocity().y == 0)
		object->SetVelocity(glm::vec2(rand() % 100, rand() % 100));

	glm::vec2 circleCentre = object->GetVelocity();
	circleCentre = glm::normalize(circleCentre);
	circleCentre *= WANDER_CIRCLE_DISTANCE;

	glm::vec2 displacement;
	displacement.x = 0;
	displacement.y = -1;
	displacement *= WANDER_RADIUS;

	int random = rand() % 100 - 50;

	m_wanderAngle += random + WANDER_ANGLE_CHANGE - (WANDER_ANGLE_CHANGE *0.5f);

	auto length = glm::length(displacement);
	displacement.x = cosf(m_wanderAngle) * length;
	displacement.y = sinf(m_wanderAngle) * length;


	glm::vec2 wanderForce = circleCentre + displacement;
	object->ApplyForce(wanderForce * m_forceStrength);
	//object->ApplyWanderForce(wanderForce);
//#endif
}


void WanderBehaviour::Draw(GameObject * object, aie::Renderer2D * renderer) {}

void WanderBehaviour::SetStrength(float strength) { m_forceStrength = strength; }

float WanderBehaviour::GetStrength() { return m_forceStrength; }
