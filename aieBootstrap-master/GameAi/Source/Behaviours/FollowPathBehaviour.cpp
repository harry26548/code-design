#include "Behaviours\FollowPathBehaviour.h"
#include <Renderer2D.h>
#include "Path.h"
#include "GameObject.h"
#include <glm\glm.hpp>
#include <glm\vec2.hpp>

FollowPathBehaviour::FollowPathBehaviour() : Behaviour(), m_path(nullptr), m_currentPathNodeIndex(0), m_strength(100), m_nodeRadius(20) {}

FollowPathBehaviour::~FollowPathBehaviour() {}

void FollowPathBehaviour::Update(GameObject * object, float deltaTime) {
	auto &path = m_path->GetPath();
	if (path.empty() == false) {
		m_currentPathNodeIndex = m_currentPathNodeIndex % path.size();
		glm::vec2 point = path[m_currentPathNodeIndex];
		if (glm::length(point - object->GetPosition()) < m_nodeRadius)
			m_currentPathNodeIndex += 1;

		m_currentPathNodeIndex = m_currentPathNodeIndex % path.size();
		point = path[m_currentPathNodeIndex];

		glm::vec2 dirToPoint = glm::normalize(point - object->GetPosition());
		object->ApplyForce(dirToPoint * m_strength);
	}
}

void FollowPathBehaviour::Draw(GameObject * object, aie::Renderer2D * renderer) {
	auto & path = m_path->GetPath();
	glm::vec2 lastPathPoint;
	int index = 0;

	for (auto iter = path.begin(); iter != path.end(); iter++, index++) {
		glm::vec2 point = (*iter);
		renderer->setRenderColour(1.f, 1.f, 1.f, 0.25f);
		renderer->drawCircle(point.x, point.y, m_nodeRadius, 2);
		unsigned int color = (index == m_currentPathNodeIndex) ? 0xFF7F00FF : 0xFFFFFFFF;
		renderer->setRenderColour(color);
		renderer->drawCircle(point.x, point.y, 4);
		if (index > 0) {
			renderer->drawLine(point.x, point.y, lastPathPoint.x, lastPathPoint.y);
		}
		lastPathPoint = point;
		renderer->setRenderColour(1.f, 1.f, 1.f, 1.f);
	}
}

Path* FollowPathBehaviour::GetPath() { return m_path; }

void FollowPathBehaviour::SetPath(Path * path) { m_path = path; }

float FollowPathBehaviour::GetStrength() { return m_strength; }

void FollowPathBehaviour::SetStrength(float strength) { m_strength = strength; }

float FollowPathBehaviour::GetNodeRadius() { return m_nodeRadius; }

void FollowPathBehaviour::SetNodeRadius(float radius) { m_nodeRadius = radius; }