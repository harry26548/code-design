#include "Behaviours/PursueBehaviour.h"
#include "GameObject.h"
#include <glm\glm.hpp>
#include <Renderer2D.h>
#include "Behaviours/SeekBehaviour.h"

PursueBehaviour::PursueBehaviour() : m_target(nullptr) {
	m_seekBehaviour = new SeekBehaviour();
	m_seekBehaviour->isOwnedByObject(false);
	m_seekBehaviour->SetForceStrength(100);
	m_seekBehaviour->SetInnerRadius(20);
}

PursueBehaviour::~PursueBehaviour() { delete m_seekBehaviour; }

void PursueBehaviour::Update(GameObject * object, float deltaTime) {
	//If there is no target, don't do anything
	if (m_target != nullptr) {
		m_futurePosition = m_target->GetPosition() + m_target->GetVelocity();
		m_seekBehaviour->SetTarget(m_futurePosition);
		float lastDistanceToTarget = glm::length(m_target->GetPosition() - m_lastPosition);
		float distanceToTarget = glm::length(m_target->GetPosition() - object->GetPosition());

		if (m_onInnerRadiusEnter&& lastDistanceToTarget > m_innerRadius && distanceToTarget <= m_innerRadius)
			m_onInnerRadiusEnter();

		if (m_onInnerRadiusExit && lastDistanceToTarget <= m_innerRadius && distanceToTarget > m_innerRadius)
			m_onInnerRadiusExit();

		if (m_onOuterRadiusEnter&& lastDistanceToTarget > m_outerRadius && distanceToTarget <= m_outerRadius)
			m_onOuterRadiusEnter();

		if (m_onOuterRadiusExit && lastDistanceToTarget <= m_outerRadius && distanceToTarget > m_outerRadius)
			m_onOuterRadiusExit();

		//object->ApplyForce(m_dirToTarget);
		m_seekBehaviour->Update(object, deltaTime);
	}
}

void PursueBehaviour::Draw(GameObject *object, aie::Renderer2D *renderer) {
	renderer->setRenderColour(1.f, 0.f, 0.f);
	//renderer->drawLine(object->GetPosition().x, object->GetPosition().y, m_futurePosition.x, m_futurePosition.y);
	float targetX = object->GetPosition().x + m_dirToTarget.x, targetY = object->GetPosition().y + m_dirToTarget.y;
	//glm::vec2 targetedPosition = m_dirToTarget - object->GetPosition();
	renderer->drawLine(object->GetPosition().x, object->GetPosition().y, targetX, targetY);
	renderer->setRenderColour(0xFFFFFFFF);
}

const glm::vec2 & PursueBehaviour::GetTargetPos() { return m_targetPos; }

void PursueBehaviour::SetTargetPos(const glm::vec2 & target) { m_targetPos = target; }

void PursueBehaviour::OnInnerRadiusEnter(std::function<void()> func) { m_onInnerRadiusEnter = func; }

void PursueBehaviour::OnInnerRadiusExit(std::function<void()> func) { m_onInnerRadiusExit = func; }

void PursueBehaviour::OnOuterRadiusEnter(std::function<void()> func) { m_onOuterRadiusEnter = func; }

void PursueBehaviour::OnOuterRadiusExit(std::function<void()> func) { m_onOuterRadiusExit = func; }

void PursueBehaviour::SetOuterRadius(float radius) { m_outerRadius = radius; }

float PursueBehaviour::GetOuterRadius() { return m_outerRadius; }

void PursueBehaviour::SetInnerRadius(float radius) { m_innerRadius = radius; }

float PursueBehaviour::GetInnerRadius() { return m_innerRadius; }

glm::vec2 PursueBehaviour::GetFuturePosition() const { return m_futurePosition; }

void PursueBehaviour::SetFuturePosition(glm::vec2 val) { m_futurePosition = val; }

GameObject* PursueBehaviour::GetTarget() { return m_target; }

void PursueBehaviour::SetTarget(GameObject * val) { m_target = val; }

float PursueBehaviour::GetForceStrength() const { return m_forceStrength; }

void PursueBehaviour::SetForceStrength(float val) { m_forceStrength = val; }