#include "Behaviours\SeekBehaviour.h"
#include "GameObject.h"
#include <Renderer2D.h>
#include <glm\vec2.hpp>
#include <glm\glm.hpp>

SeekBehaviour::SeekBehaviour() : m_innerRadius(0.f), m_outerRadius(0.f) {}

SeekBehaviour::~SeekBehaviour() {}

void SeekBehaviour::Update(GameObject *object, float deltaTime) {
	float lastDistanceToTarget = glm::length(m_targetPos - m_lastPosition);
	float distanceToTarget = glm::length(m_targetPos - object->GetPosition());

	if (m_onInnerRadiusEnter&& lastDistanceToTarget > m_innerRadius && distanceToTarget <= m_innerRadius)
		m_onInnerRadiusEnter();

	if (m_onInnerRadiusExit && lastDistanceToTarget <= m_innerRadius && distanceToTarget > m_innerRadius)
		m_onInnerRadiusExit();

	if (m_onOuterRadiusEnter&& lastDistanceToTarget > m_outerRadius && distanceToTarget <= m_outerRadius)
		m_onOuterRadiusEnter();

	if (m_onOuterRadiusExit && lastDistanceToTarget <= m_outerRadius && distanceToTarget > m_outerRadius)
		m_onOuterRadiusExit();

	glm::vec2 dirToTarget = glm::normalize(m_targetPos - object->GetPosition()) * m_forceStrength;
	object->ApplyForce(dirToTarget);

	m_lastPosition = object->GetPosition();
}

void SeekBehaviour::Draw(GameObject * object, aie::Renderer2D * renderer) {
	renderer->drawBox(m_targetPos.x, m_targetPos.y, 4, 4);
	renderer->setRenderColour(1.f, 1.f, 1.0f, 0.5f);
	renderer->drawCircle(m_targetPos.x, m_targetPos.y, m_innerRadius);
	renderer->drawCircle(m_targetPos.x, m_targetPos.y, m_outerRadius);
	renderer->setRenderColour(1.f, 1.f, 1.0f, 1.f);
}

const glm::vec2 & SeekBehaviour::GetTarget() { return m_targetPos; }

void SeekBehaviour::SetTarget(const glm::vec2 & target) { m_targetPos = target; }

void SeekBehaviour::SetForceStrength(float strength) { m_forceStrength = strength; }

float SeekBehaviour::GetForceStrength() { return m_forceStrength; }

void SeekBehaviour::SetOuterRadius(float radius) { m_outerRadius = radius; }

float SeekBehaviour::GetOuterRadius() { return m_outerRadius; }

void SeekBehaviour::SetInnerRadius(float radius) { m_innerRadius = radius; }

float SeekBehaviour::GetInnerRadius() { return m_innerRadius; }

void SeekBehaviour::OnInnerRadiusEnter(std::function<void()> func) { m_onInnerRadiusEnter = func; }

void SeekBehaviour::OnInnerRadiusExit(std::function<void()> func) { m_onInnerRadiusExit = func; }

void SeekBehaviour::OnOuterRadiusEnter(std::function<void()> func) { m_onOuterRadiusEnter = func; }

void SeekBehaviour::OnOuterRadiusExit(std::function<void()> func) { m_onOuterRadiusExit = func; }