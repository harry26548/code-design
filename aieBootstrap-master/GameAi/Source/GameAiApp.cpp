#include "GameAiApp.h"
#include <Texture.h>
#include <Font.h>
#include <Input.h>
#include "GameObject.h"
#include "Behaviours\KeyboardBehaviour.h"
#include "Entities\Player.h"
#include "Graph\Graph2DRenderer.h"
#include "Graph\Graph.hpp"
#include <glm\glm.hpp>
#include "Entities\NPC.h"
#include "hb_utility.h"
#include "Entities\EntitiesManager.h"

GameAiApp::GameAiApp() {}

GameAiApp::~GameAiApp() {}

bool GameAiApp::startup() {
	m_2dRenderer = new aie::Renderer2D();
	m_font = new aie::Font("./font/consolas.ttf", FONT_SIZE);

	m_graph = new Graph2D();
	m_graphRenderer = new Graph2DRenderer();
	m_graphRenderer->SetGraph(m_graph);

	SetUpGraph();

	m_entities = new EntitiesManager();
	m_entities->CreatePlayer(glm::vec2(getWindowWidth() * 0.5f, getWindowHeight() * 0.5f), m_graph);
	m_entities->CreateNPC(glm::vec2(getWindowWidth() * 0.4f, getWindowHeight() * 0.4f), m_graph);

	return true;
}

void GameAiApp::shutdown() {
	delete m_graph;
	delete m_entities;
	delete m_graphRenderer;
	delete m_font;
	delete m_2dRenderer;
}

void GameAiApp::update(float deltaTime) {
	// input example
	aie::Input* input = aie::Input::getInstance();
	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE))
		quit();
	UpdateGraph(deltaTime);
}

void GameAiApp::draw() {
	// wipe the screen to the background color
	clearScreen();

	// begin drawing sprites
	m_2dRenderer->begin();
	DrawGraph();

	// draw your stuff here!
	m_entities->Draw(m_2dRenderer);

	// output some text, uses the last used color
	m_2dRenderer->drawText(m_font, "Press ESC to quit. Press T to Wander. Middle Mouse on 2 nodes to select and path find", 0, 0);

	// done drawing sprites
	m_2dRenderer->end();
}

void GameAiApp::SetUpGraph() {
	m_graphToMouseDistance = GRAPH_SPACING;
	for (int y = 0; y < GRAPH_NUM_ROWS; y++) {
		for (int x = 0; x < GRAPH_NUM_COLS; x++) {
			m_graph->AddNode(glm::vec2(x * GRAPH_SPACING + GRAPH_XOFFSET, y * GRAPH_SPACING + GRAPH_YOFFSET));
		}
	}
	auto nodes = m_graph->GetNodes();
	for (auto iter = nodes.begin(); iter != nodes.end(); iter++) {
		auto node = (*iter);
		std::vector<Graph2D::Node *> surroundingNodes;
		m_graph->GetNearbyNodes(node->data, GRAPH_EDGE_MAX_LENGTH, surroundingNodes);

		for (auto connectionsIter = surroundingNodes.begin(); connectionsIter != surroundingNodes.end(); connectionsIter++) {
			if ((*connectionsIter) == node)
				continue;
			float dist = glm::length((*connectionsIter)->data - node->data);
			m_graph->AddEdge(node, (*connectionsIter), true, dist);
		}
	}
}

void GameAiApp::UpdateGraph(float deltaTime) {
	aie::Input* input = aie::Input::getInstance();
	if (input->wasKeyPressed(aie::INPUT_KEY_SPACE)) {
		std::vector<Graph2D::Node *> surroundingNodes;
		m_graph->GetNearbyNodes(m_mousePos, GRAPH_EDGE_MAX_LENGTH, surroundingNodes);
		auto addedNode = m_graph->AddNode(m_mousePos);
		for (register auto iter = surroundingNodes.begin(); iter != surroundingNodes.end(); iter++) {
			m_graph->AddEdge((*iter), m_graph->GetNodeById(addedNode), true, glm::length(m_graph->GetNodeById(addedNode)->data - (*iter)->data));
		}
	}

	m_entities->Update(deltaTime);
	m_graphRenderer->Update(deltaTime);
	int mouseX, mouseY;
	input->getMouseXY(&mouseX, &mouseY);
	m_mousePos = glm::vec2(mouseX, mouseY);

	//Wrapping player
	for (register auto iter = m_entities->GetObjectsVector().begin(); iter != m_entities->GetObjectsVector().end(); iter++) {
		const glm::vec2 &pos = (*iter)->GetPosition();

		//Wrapping on the left and right
		if (pos.x < 0) (*iter)->SetPosition(glm::vec2(getWindowWidth(), pos.y));
		if (pos.x > getWindowWidth()) (*iter)->SetPosition(glm::vec2(0, pos.y));

		//Wrapping the top and bottom
		if (pos.y < 0) (*iter)->SetPosition(glm::vec2(pos.x, getWindowHeight()));
		if (pos.y > getWindowHeight()) (*iter)->SetPosition(glm::vec2(pos.x, 0));
	}
}

void GameAiApp::DrawGraph() {
	m_graphRenderer->Draw(m_2dRenderer);

	std::vector<Graph2D::Node *> surroundingNodes;
	m_graph->GetNearbyNodes(m_mousePos, GRAPH_EDGE_MAX_LENGTH, surroundingNodes);
	for (register auto iter = surroundingNodes.begin(); iter != surroundingNodes.end(); iter++) {
		m_2dRenderer->drawLine(m_mousePos.x, m_mousePos.y, (*iter)->data.x, (*iter)->data.y);
	}
}