#include "Graph\Graph2DRenderer.h"
#include <Renderer2D.h>

Graph2DRenderer::Graph2DRenderer() {}

Graph2DRenderer::~Graph2DRenderer() {}

void Graph2DRenderer::Draw(aie::Renderer2D *renderer) {
	auto &nodes = m_graph->GetNodes();
	renderer->setRenderColour(1.f, 1.f, 1.f, 0.25f);
 
	for (auto iter = nodes.begin(); iter != nodes.end(); iter++) {
		Graph2D::Node *node = (*iter);
		renderer->drawCircle(node->data.x, node->data.y, 8);
	}

	for (auto nIter = nodes.begin(); nIter != nodes.end(); nIter++) {
		Graph2D::Node *nodeA = (*nIter);
		for (auto eIter = nodeA->edges.begin(); eIter != nodeA->edges.end(); eIter++) {
			Graph2D::Node *nodeB = (*eIter).to;
			renderer->drawLine(nodeA->data.x, nodeA->data.y, nodeB->data.x, nodeB->data.y, 1.f);
		}
	}
	renderer->setRenderColour(1.f, 1.f, 1.f, 1.f);
}

void Graph2DRenderer::Update(float dt) {

}

void Graph2DRenderer::SetGraph(Graph2D *graph) { m_graph = graph; }

Graph2D *Graph2DRenderer::GetGraph() { return m_graph; }