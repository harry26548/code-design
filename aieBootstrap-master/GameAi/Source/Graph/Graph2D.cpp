#include "Graph\Graph2D.h"
#include <Renderer2D.h>
#include <glm\glm.hpp>

void Graph2D::GetNearbyNodes(glm::vec2 pos, float radius, std::vector<Graph2D::Node *> &out_collection) {
	for (auto iter = m_nodes.begin(); iter != m_nodes.end(); iter++) {
		Graph2D::Node *node = (*iter);
		float dist = glm::length(node->data - pos);
		if (dist < radius) {
			out_collection.push_back(node);
		}
	}
}