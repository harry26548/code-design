#include "Entities/EntitiesManager.h"
#include "Graph/Graph2D.h"
#include <glm/vec2.hpp>
#include "Entities/NPC.h"
#include "Entities/Player.h"
#include <glm/glm.hpp>
#include "hb_utility.h"

EntitiesManager::EntitiesManager() {}

EntitiesManager::~EntitiesManager() {
	for (auto iter = m_gameObjects.begin(); iter != m_gameObjects.end(); iter++) {
		delete (*iter);
	}
	m_gameObjects.clear();
}

void EntitiesManager::CreatePlayer(glm::vec2 pos, Graph2D* graph) {
	Player *p = new Player();
	p->SetPosition(pos);
	p->SetGraph(graph);
	m_gameObjects.push_back(p);
}

void EntitiesManager::CreateNPC(glm::vec2 pos, Graph2D* graph) {
	NPC *n = new NPC(GetPlayer());
	n->SetPosition(pos);
	n->SetGraph(graph);
	m_gameObjects.push_back(n);
}


std::vector<GameObject *>& EntitiesManager::GetObjectsVector() { return m_gameObjects; }

void EntitiesManager::GetNearbyEntities(GameObject* object, float radius, std::vector<GameObject *> &out_collection) {
	for (register auto iter = m_gameObjects.begin(); iter != m_gameObjects.end(); iter++) {
		GameObject *gameObject = (*iter);
		float dist = glm::length(gameObject->GetPosition() - object->GetPosition());
		if (dist < radius) {
			// Don't include itself
			if (gameObject != object) {
				out_collection.push_back(gameObject);
			}
		}
	}
}

GameObject* EntitiesManager::GetPlayer() {
	for (register auto iter = m_gameObjects.begin(); iter != m_gameObjects.end(); iter++) {
		Player* p = dynamic_cast<Player*>(*iter);
		if (p != nullptr) {
			return p;
		}
	}
	return nullptr;
}

void EntitiesManager::Update(float dt) {
	// Call clean up function 
	for (register auto iter = m_gameObjects.begin(); iter != m_gameObjects.end(); ++iter) {
		// Object is marked for death
		if (!(*iter)->IsAlive()) {
			delete *iter;				// Clean dynamically allocated memory
			// Remove from vector
			// If there's only one object, erasing it will put it onto invalid memory so check if its at the end and break out before incrementing.
			iter = m_gameObjects.erase(iter);
			if (iter == m_gameObjects.end()) {
				break;
			}
		}
		// Update object as normal
		(*iter)->Update(dt);
	}

	std::vector<GameObject *> nearbyObjects;
	GetNearbyEntities(GetPlayer(), 20, nearbyObjects);
	if (!nearbyObjects.empty()) {
		for (register auto iter = nearbyObjects.begin(); iter != nearbyObjects.end(); ++iter) {
			Player* p = dynamic_cast<Player*>(*iter);
			NPC* n = dynamic_cast<NPC*>(*iter);
			if (p != nullptr) {
				p->ApplyDamage(NPC_DAMAGE);
			}
			if (n != nullptr) {
				n->ApplyDamage(PLAYER_DAMAGE);
			}
		}
	}
}

void EntitiesManager::Draw(aie::Renderer2D* renderer) {
	for (register auto iter = m_gameObjects.begin(); iter != m_gameObjects.end(); iter++) {
		(*iter)->Draw(renderer);
	}
}