#include "Entities\Player.h"
#include "Behaviours\SeekBehaviour.h"
#include "Behaviours\KeyboardBehaviour.h"
#include "Behaviours\FollowPathBehaviour.h"
#include "Behaviours\WanderBehaviour.h"
#include "Path.h"
#include <Input.h>
#include <Renderer2D.h>
#include "Graph\Graph2D.h"
#include "Pathfinding\Pathfinding.h"
#include <iostream>

Player::Player() : GameObject(), m_pathfinder(nullptr) {

	m_graph = nullptr;
	m_startNode = nullptr;
	m_endNode = nullptr;

	// Keyboard Behaviour setup and properties
	m_keyboardBehaviour = new KeyboardBehaviour;
	m_keyboardBehaviour->isOwnedByObject(false);

	// Seek Behaviour setup and properties
	m_seekBehaviour = new SeekBehaviour();
	m_seekBehaviour->isOwnedByObject(false);
	m_seekBehaviour->SetForceStrength(100);
	m_seekBehaviour->SetInnerRadius(20);
	m_seekBehaviour->OnInnerRadiusEnter([this]() {
		SetBehaviour(m_keyboardBehaviour);
	});

	// Flee Behaviour setup and properties
	m_fleeBehaviour = new SeekBehaviour();
	m_fleeBehaviour->isOwnedByObject(false);
	m_fleeBehaviour->SetForceStrength(-100);
	m_fleeBehaviour->SetOuterRadius(100);
	m_fleeBehaviour->OnOuterRadiusExit([this]() {
		SetBehaviour(m_keyboardBehaviour);
	});

	m_path = new Path();

	// Follow Path Behaviour setup and properties
	m_followPathBehaviour = new FollowPathBehaviour();
	m_followPathBehaviour->isOwnedByObject(false);
	m_followPathBehaviour->SetPath(m_path);

	m_wanderBehaviour = new WanderBehaviour();
	m_wanderBehaviour->isOwnedByObject(false);
	m_wanderBehaviour->SetStrength(20);

	SetHealth(100);

	SetBehaviour(m_keyboardBehaviour);

	SetFriction(1.f);
	SetVelocity(glm::vec2(1.f, 1.f));
};

Player::~Player() {
	SetBehaviour(nullptr);
	delete m_path;
	delete m_keyboardBehaviour;
	delete m_seekBehaviour;
	delete m_fleeBehaviour;
	delete m_pathfinder;
	delete m_wanderBehaviour;
}

void Player::Update(float deltaTime) {
	aie::Input *input = aie::Input::getInstance();
	int mx, my;

	input->getMouseXY(&mx, &my);

	if (input->wasMouseButtonPressed(aie::INPUT_MOUSE_BUTTON_LEFT)) {
		m_seekBehaviour->SetTarget(glm::vec2(mx, my));
		SetBehaviour(m_seekBehaviour);
	} else if (input->wasMouseButtonPressed(aie::INPUT_MOUSE_BUTTON_RIGHT)) {
		m_fleeBehaviour->SetTarget(glm::vec2(mx, my));
		SetBehaviour(m_fleeBehaviour);
	} else if (input->wasKeyPressed(aie::INPUT_KEY_T)) { SetBehaviour(m_wanderBehaviour); }

	if (GetBehaviour() != m_keyboardBehaviour && input->getPressedKeys().empty() == false) {
		if (!input->wasKeyPressed(aie::INPUT_KEY_T))
			SetBehaviour(m_keyboardBehaviour);
	}
	GameObject::Update(deltaTime);
}

void Player::Draw(aie::Renderer2D * renderer) {
	if (m_startNode != nullptr) renderer->drawCircle(m_startNode->data.x, m_startNode->data.y, 4);
	if (m_endNode != nullptr) renderer->drawCircle(m_endNode->data.x, m_endNode->data.y, 4);
	if (m_pathfinder != nullptr)
		m_pathfinder->Draw(renderer);
	GameObject::Draw(renderer);
}

Graph2D * Player::GetGraph() const { return m_graph; }

void Player::SetGraph(Graph2D * graph) { m_graph = graph; }