#include "Entities\NPC.h"
#include "Behaviours\PursueBehaviour.h"
#include "Behaviours\SeekBehaviour.h"
#include "hb_utility.h"
#include "Pathfinding\Pathfinding.h"
#include "Behaviours\FollowPathBehaviour.h"
#include "Graph\Graph2D.h"
#include "Path.h"

NPC::NPC(GameObject *player) : GameObject() {
	SetFriction(1);

	m_graph = nullptr;
	m_startNode = nullptr;
	m_endNode = nullptr;
	m_player = player;

	m_pursueBehaviour = new PursueBehaviour;
	m_pursueBehaviour->isOwnedByObject(false);
	m_pursueBehaviour->SetTarget(player);
	m_pursueBehaviour->SetForceStrength(20.f);
	m_pursueBehaviour->SetOuterRadius(OUTER_RADIUS);
	m_pursueBehaviour->OnOuterRadiusEnter([this]() {
		SetIsNearby(true);
	});

	m_fleeBehaviour = new SeekBehaviour;
	m_fleeBehaviour->isOwnedByObject(false);
	m_fleeBehaviour->SetForceStrength(-500.f);
	m_fleeBehaviour->SetOuterRadius(OUTER_RADIUS);
	m_fleeBehaviour->SetTarget(player->GetPosition());
	m_fleeBehaviour->OnOuterRadiusExit([this]() {
		SetBehaviour(m_followPathBehaviour);
		SetIsNearby(false);
	});

	m_path = new Path();

	// Follow Path Behaviour setup and properties
	m_followPathBehaviour = new FollowPathBehaviour();
	m_followPathBehaviour->isOwnedByObject(false);
	m_followPathBehaviour->SetPath(m_path);

	SetBehaviour(m_followPathBehaviour);
	SetHealth(500);
}

NPC::~NPC() {
	SetBehaviour(nullptr);
	delete m_pursueBehaviour;
};

void NPC::Update(float deltaTime) {
	if (m_health < 25) {
		SetBehaviour(m_fleeBehaviour);
		m_fleeBehaviour->SetTarget(m_player->GetPosition());
	}
	if (IsNearby() == false) {
		if (m_health < 500)
			m_health += 5;
	}

	std::vector<Graph2D::Node *> nearbyNodes;
	m_graph->GetNearbyNodes(GetPosition(), 30, nearbyNodes);

	if (!nearbyNodes.empty()) {
		if (m_startNode == nullptr) {
			// The Start node should be the closest one, which would be index 0
			m_startNode = nearbyNodes[0];
		} else if (m_endNode == nullptr) {
			std::vector<Graph2D::Node *> nearbyNodesTarget;
			m_graph->GetNearbyNodes(m_player->GetPosition(), 50, nearbyNodesTarget);
			m_endNode = nearbyNodesTarget[0];
			m_pathfinder = new Pathfinder(m_graph);
			m_pathfinder->FindPath(m_startNode, [this](Graph2D::Node *n) {return n == m_endNode; });
			while (m_pathfinder->PathFound() == false) {
				m_pathfinder->UpdateSearch();
			}
			m_path->Clear();
			Path &p = m_pathfinder->GetPath();
			auto pathPoints = p.GetPath();
			for (register auto iter = pathPoints.rbegin(); iter != pathPoints.rend(); iter++) {
				m_path->PushPathSegment((*iter));
			}
			SetBehaviour(m_followPathBehaviour);
		} else {
			m_startNode = nullptr;
			m_endNode = nullptr;
			delete m_pathfinder;
			m_pathfinder = nullptr;
		}
	}

	GameObject::Update(deltaTime);
};

void NPC::Draw(aie::Renderer2D * renderer) { GameObject::Draw(renderer); }

Graph2D * NPC::GetGraph() const { return m_graph; }

void NPC::SetGraph(Graph2D * graph) { m_graph = graph; }
