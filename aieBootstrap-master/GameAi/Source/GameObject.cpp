#include "GameObject.h"
#include <Renderer2D.h>
#include "Behaviours\Behaviour.h"
#include "hb_utility.h"

GameObject::GameObject() : m_friction(0.f), m_behaviour(nullptr), m_isAlive(true) {}

GameObject::~GameObject() { SetBehaviour(nullptr); }

void GameObject::Update(float deltaTime) {
	if (m_behaviour != nullptr)
		m_behaviour->Update(this, deltaTime);
	SimulatePhysics(deltaTime);
}

void GameObject::Draw(aie::Renderer2D * renderer) {
	glm::vec2 targetHeading = m_position + m_velocity;
	renderer->drawCircle(m_position.x, m_position.y, 8);
	renderer->setRenderColour(0xFF7F00FF);
	renderer->drawLine(m_position.x, m_position.y, targetHeading.x, targetHeading.y, 2);
	renderer->setRenderColour(0xFFFFFFFF);

	if (m_behaviour != nullptr) {
		m_behaviour->Draw(this, renderer);
	}
}

void GameObject::SimulatePhysics(float deltaTime) {
	//Physics Logic
	ApplyForce(m_friction * -m_velocity);
	m_velocity += m_acceleration * deltaTime;

	m_position += m_velocity * deltaTime;
	m_acceleration = glm::vec2(0, 0);
}

void GameObject::ApplyForce(const glm::vec2 & force) { m_acceleration += force; }

void GameObject::ApplyDamage(const int & health) { m_health -= health; }

glm::vec2 & GameObject::GetPosition() { return m_position; }

glm::vec2 & GameObject::GetVelocity() { return m_velocity; }

float GameObject::GetFriction() { return m_friction; }

Behaviour * GameObject::GetBehaviour() { return m_behaviour; }

int GameObject::GetHealth() { return m_health; }

bool GameObject::IsAlive() { return m_isAlive; }

bool GameObject::IsNearby() { return m_isNearby; }

void GameObject::SetPosition(const glm::vec2 pos) { m_position = pos; }

void GameObject::SetVelocity(const glm::vec2 vel) { m_velocity = vel; }

void GameObject::SetFriction(float fric) { m_friction = fric; }

void GameObject::SetBehaviour(Behaviour * behaviour) {
	if (m_behaviour && m_behaviour->isOwnedByObject())
		delete m_behaviour;
	m_behaviour = behaviour;
}

void GameObject::SetHealth(int health) { m_health = health; }

void GameObject::SetIsAlive(bool alive) { m_isAlive = alive; }

void GameObject::SetIsNearby(bool near) { m_isNearby = near; }
