#include "BinaryTree.h"
#include "TreeNode.h"
#include <Renderer2D.h>
#include <iostream>
#include <cstdlib>
#include <assert.h>
#include <list>

using namespace std;

BinaryTree::BinaryTree()
{
    m_pRoot = nullptr;
}

BinaryTree::~BinaryTree()
{
	while(m_pRoot)
	{
		remove(m_pRoot->getData());
	}
}

// Return whether the tree is empty
bool BinaryTree::isEmpty() const 
{ 
	return (m_pRoot == nullptr);
}

// Insert a new element into the tree.
// Smaller elements are placed to the left, larger onces are placed to the right.
void BinaryTree::insert(int a_nValue)
{

	TreeNode *current, *temp;
	TreeNode* insertedNode = new TreeNode(a_nValue, 'r');

	current = m_pRoot;
	temp = NULL;
	if (current == nullptr)
	{
		current = insertedNode;
		insertedNode->setParent(nullptr);
	} else {
		while (current != NULL)
		{
			temp = current;
			if (current->getData() < insertedNode->getData())
				current->setRight(current);
			else
				current->setRight(current);
		}

		insertedNode->setParent(temp);
		if (temp->getData() < insertedNode->getData())
			temp->setRight(insertedNode);
		else
			temp->setLeft(insertedNode);
	}

	insertBalance(insertedNode);	

#if 0
	TreeNode* pParent = nullptr;

	// If this is a new tree then store the new root node.
	if(isEmpty())
	{
		m_pRoot = new TreeNode(a_nValue);
	}
	else
	{
		TreeNode* pCurrent = m_pRoot;
        
		// Find the Node's parent
		while(pCurrent)
		{
			pParent = pCurrent;
			if (a_nValue == pCurrent->getData())
			{
				return;
			} else if (a_nValue > pCurrent->getData()) {
				pCurrent = pCurrent->getRight();
			} else {
				pCurrent = pCurrent->getLeft();
			}
		}

		if (a_nValue > pParent->getData())
		{
			pParent->setRight(new TreeNode(a_nValue, 'r'));
			insertBalance(pParent->getRight);
		} else {
			pParent->setLeft(new TreeNode(a_nValue, 'r'));
			insertBalance(pParent->getLeft);
		}
	}
#endif
}

void BinaryTree::insertBalance(TreeNode *a_insertedNode)
{
	TreeNode *tempName;

	if (m_pRoot == a_insertedNode)
	{
		a_insertedNode->setColor('b');
		return;
	}
	while (a_insertedNode->getParent() != NULL && a_insertedNode->getParent()->getColor() == 'r')
	{
		TreeNode *grandParent = a_insertedNode->getParent()->getParent();
		if (grandParent->getLeft() == a_insertedNode->getParent())
		{
			if (grandParent->getRight() != NULL)
			{
				tempName = grandParent->getRight();
				if (tempName->getColor() == 'r')
				{
					a_insertedNode->getParent()->setColor('b');
					tempName->setColor('b');
					grandParent->setColor('r');
					a_insertedNode = grandParent;
				}
			} else {
				if (a_insertedNode->getParent()->getRight() == a_insertedNode)
				{
					a_insertedNode = a_insertedNode->getParent();
					leftRotate(a_insertedNode);
				}
				a_insertedNode->getParent()->setColor('b');
				grandParent->setColor('r');
				rightRotate(grandParent);
			}
		} else {
			if (grandParent->getLeft() != nullptr)
			{
				tempName = grandParent->getLeft();
				if (tempName->getColor() == 'r')
				{
					a_insertedNode->getParent()->setColor('b');
					tempName->setColor('b');
					grandParent->setColor('r');
					a_insertedNode = grandParent;
				}
			} else {
				if (a_insertedNode->getParent()->getLeft() == a_insertedNode)
				{
					a_insertedNode = a_insertedNode->getParent();
					rightRotate(a_insertedNode);
				}
				a_insertedNode->getParent()->setColor('b');
				grandParent->setColor('r');
				leftRotate(grandParent);
			}
		}
		m_pRoot->setColor('b');
	}
}

TreeNode* BinaryTree::find(int a_nValue)
{
	TreeNode* pCurrent = nullptr;
	TreeNode* pParent = nullptr;

	return findNode(a_nValue, &pCurrent, &pParent) ? pCurrent: nullptr;
}

void BinaryTree::leftRotate(TreeNode * rotated)
{
	if (rotated->getRight() == NULL)
	{
		return;
	} else {
		TreeNode *y = rotated->getRight();
		if (y->getLeft() != NULL)
		{
			rotated->setRight(y->getLeft());
			y->getLeft()->setParent(rotated);
		} else {
			rotated->setRight(NULL);

			if (rotated->getParent() != NULL)
				y->setParent(rotated->getParent());

			if (rotated->getParent() == NULL)
				m_pRoot = y;
			else
			{
				if (rotated == rotated->getParent()->getLeft())
					rotated->getParent()->setLeft(y);
				else
					rotated->getParent()->setRight(y);
			}
			y->setLeft(rotated);
			rotated->setParent(y);
		}
	}
}

void BinaryTree::rightRotate(TreeNode * rotated)
{
	if (rotated->getLeft() == NULL)
		return;
	else
	{
		TreeNode *y = rotated->getLeft();

		if (y->getRight() != NULL)
		{
			rotated->setLeft(y->getRight());
			y->getRight()->setParent(rotated);
		}
		else
			rotated->setLeft(NULL);
		if (rotated->getParent() != NULL)
			y->setParent(rotated->getParent());
		if (rotated->getParent() == NULL)
			m_pRoot = y;
		else
		{
			if (rotated == rotated->getParent()->getLeft())
				rotated->getParent()->setLeft(y);
			else
				rotated->getParent()->setRight(y);
		}
		y->setRight(rotated);
		rotated->setParent(y);
	}
}

TreeNode * BinaryTree::successor(TreeNode * p)
{
	TreeNode *y = NULL;
	if (p->getLeft() != NULL)
	{
		y = p->getLeft();
		while (y->getLeft() != NULL)
			y = y->getLeft();
	} else {
		y = p->getLeft();
		while (y->getLeft() != NULL)
			y = y->getLeft();
	}
	return y;
}

bool BinaryTree::findNode(int a_nSearchValue, TreeNode** ppOutNode, TreeNode** ppOutParent)
{
	*ppOutNode = nullptr;
	*ppOutParent = nullptr;

	//Locate the element
	if(isEmpty())
	{
		return false;
	}
    
	TreeNode* pCurrent = m_pRoot;
	TreeNode* pParent = nullptr;
    
	while(pCurrent)
	{
		if(a_nSearchValue == pCurrent->getData())
		{
			*ppOutNode = pCurrent;
			*ppOutParent = pParent;
			return true;
		}
		else
		{
			pParent = pCurrent;

			if(a_nSearchValue > pCurrent->getData())
				pCurrent = pCurrent->getRight();
			else
				pCurrent = pCurrent->getLeft();
		}
	}

	return false;
}

void BinaryTree::remove(int a_nValue)
{
	//TreeNode* pCurrent = nullptr;
	//TreeNode* pParent = nullptr;

	if (m_pRoot == nullptr)
	{
		std::cout << "Emptry Tree" << std::endl;
		return;
	}

	TreeNode *p;
	p = m_pRoot;
	TreeNode *y = NULL;
	TreeNode *q = NULL;

	int found = 0;

	while (p != NULL && found == 0)
	{
		if (p->getData() == a_nValue)
			found = 1;
		if (found == 0)
		{
			if (p->getData() < a_nValue)
				p = p->getRight();
			else
				p = p->getLeft();
		}
	}
	if (found == 0)
	{
		return;
	} else {
		if (p->getLeft() == NULL || p->getRight() == NULL)
			y = p;
		else
			y = successor(p);
		if (y->getLeft() != NULL)
			q = y->getLeft();
		else
		{
			if (y->getRight() != NULL)
				q = y->getRight();
			else
				q = NULL;
		}
		if (q != NULL)
			q->setParent(y->getParent());
		if (y->getParent() == NULL)
			m_pRoot = q;
		else
		{
			if (y == y->getParent()->getLeft())
				y->getParent()->setLeft(q);
			else
				y->getParent()->setRight(q);
		}
		if (y != p)
		{
			p->setColor(y->getColor());
			p->setData(y->getData());
		}
		if (y->getColor() == 'b')
			removeBalance(q);
	}
}

void BinaryTree::removeBalance(TreeNode *a_removedNode)
{
	TreeNode *s;
	while (a_removedNode != m_pRoot && a_removedNode->getColor() == 'b')
	{
		if (a_removedNode->getParent()->getLeft() == a_removedNode)
		{
			s = a_removedNode->getParent()->getRight();
			if (s->getColor() == 'r')
			{
				s->setColor('r');
				a_removedNode->getParent()->setColor('r');
				leftRotate(a_removedNode->getParent());
				s = a_removedNode->getParent()->getRight();
			}
			if (s->getRight()->getColor() == 'b' && s->getLeft()->getColor() == 'b')
			{
				s->setColor('r');
				a_removedNode = a_removedNode->getParent();

			} else {
				if (s->getRight()->getColor() == 'b')
				{
					s->getLeft()->setColor('b');
					s->setColor('r');
					rightRotate(s);
					s = a_removedNode->getParent()->getRight();
				}
				s->setColor(a_removedNode->getParent()->getColor());
				a_removedNode->getParent()->setColor('b');
				s->getRight()->setColor('b');
				leftRotate(a_removedNode->getParent());
				a_removedNode = m_pRoot;
			}
		} else {
			s = a_removedNode->getParent()->getLeft();
			if (s->getColor() == 'r')
			{
				s->setColor('b');
				a_removedNode->getParent()->setColor('r');
				rightRotate(a_removedNode->getParent());
				s = a_removedNode->getParent()->getLeft();
			}
			if (s->getLeft()->getColor() == 'b' && s->getRight()->getColor() == 'b')
			{
				s->setColor('r');
				a_removedNode = a_removedNode->getParent();
			} else {
				if (s->getLeft()->getColor() == 'b')
				{
					s->getRight()->setColor('b');
					s->setColor('r');
					leftRotate(s);
					s = a_removedNode->getParent()->getLeft();
				}
				s->setColor(a_removedNode->getParent()->getColor());
				a_removedNode->getParent()->setColor('b');
				s->getLeft()->setColor('b');
				rightRotate(a_removedNode->getParent());
				a_removedNode = m_pRoot;
			}
		}
		a_removedNode->setColor('b');
		m_pRoot->setColor('b');
	}
}

void BinaryTree::printOrdered()
{
	printOrderedRecurse(m_pRoot);
	cout << endl;
}

void BinaryTree::printOrderedRecurse(TreeNode* pNode)
{
    if(pNode)
    {
        if(pNode->hasLeft()) 
			printOrderedRecurse(pNode->getLeft());
        
		cout << " " << pNode->getData() << " ";
        
		if(pNode->hasRight())
			printOrderedRecurse(pNode->getRight());
    }
}

void BinaryTree::printUnordered()
{
    printUnorderedRecurse(m_pRoot);
	cout << endl;
}

void BinaryTree::printUnorderedRecurse(TreeNode* pNode)
{
    if(pNode)
    {
        cout << " " << pNode->getData() << " ";

        if(pNode->hasLeft()) 
			printUnorderedRecurse(pNode->getLeft());

        if(pNode->hasRight()) 
			printUnorderedRecurse(pNode->getRight());
    }
}

void BinaryTree::draw(aie::Renderer2D* renderer, TreeNode* selected)
{
	draw(renderer, m_pRoot, 640, 680, 640, selected);
}

void BinaryTree::draw(aie::Renderer2D* renderer, TreeNode* pNode, int x, int y, int horizontalSpacing, TreeNode* selected) {
	horizontalSpacing /= 2;

	bool isFinished = false;

	std::list<TreeNode*> drawingNodeList;
	drawingNodeList.push_back(pNode);

	while (isFinished != true) {
			
			drawingNodeList.front()->draw(renderer, x, y, (selected == pNode));
		
		if (drawingNodeList.front() != nullptr) {
			if (drawingNodeList.front()->hasLeft()) {
				renderer->setRenderColour(1, 0, 0);
				renderer->drawLine((float)x, (float)y, (float)(x - horizontalSpacing), (float)(y - 80));
				draw(renderer, drawingNodeList.front()->getLeft(), x - horizontalSpacing, y - 80, horizontalSpacing, selected);
			}

			if (drawingNodeList.front()->hasRight()) {
				renderer->setRenderColour(1, 0, 0);
				renderer->drawLine((float)x, (float)y, (float)(x + horizontalSpacing), (float)(y - 80));
				draw(renderer, drawingNodeList.front()->getRight(), x + horizontalSpacing, y - 80, horizontalSpacing, selected);
			}

		}
			drawingNodeList.push_back(drawingNodeList.front()->getLeft());
			drawingNodeList.push_back(drawingNodeList.front()->getRight());

			drawingNodeList.pop_front();
	}
}