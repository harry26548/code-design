// Hashing.cpp : Defines the entry point for the console application.
//
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "Hashing.h"
#include "stdafx.h"

//unsigned int RSHash_createHash ()
//{
//	unsigned int hash;
//
//
//	return hash;
//}

unsigned int BadHash_CreateHash() {

	char* test = new char[6];
	test = "HHHHH";

	return Hashing::badHash(test, 5);
}

bool BadHash_AddToTable() {
	Hashing::HashTable* testTable = new Hashing::HashTable();

	unsigned int hash;

	char* bh_0 = new char[4];
	bh_0 = "The"; //289
	hash = Hashing::badHash(bh_0, 3);
	testTable->AddEntry(hash);

	char* bh_1 = new char[6];
	bh_1 = "Memes"; //503
	hash = Hashing::badHash(bh_1, 3);
	testTable->AddEntry(hash);

	char* bh_2 = new char[5];
	bh_2 = "Dank"; //382 
	hash = Hashing::badHash(bh_2, 3);
	testTable->AddEntry(hash);

	char* bh_3 = new char[5];
	bh_3 = "Lmao"; //393
	hash = Hashing::badHash(bh_3, 3);
	testTable->AddEntry(hash);

	for (auto iter = testTable->m_table.begin(); iter != testTable->m_table.end() - 1; iter++) {
		if (!(iter < iter + 1)) {
			return false;
		}
	}

	return true;
}

TEST_CASE("BadHash", "Hash") {
	REQUIRE(BadHash_CreateHash() == 360);
	REQUIRE(BadHash_AddToTable() == true);

}