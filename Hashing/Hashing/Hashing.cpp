#include "Hashing.h"
namespace Hashing {

	unsigned int Hashing::badHash(const char * data, unsigned int length) {
		unsigned int hash = 0;
		for (unsigned int i = 0; i < length; ++i) {
			hash += data[i];
		}
		return hash;
	}

	// Taken from http://www.partow.net/programming/hashfunctions/ and edited to suit projects purpose.
	unsigned int Hashing::RSHash(const char * data, unsigned int length) {
		unsigned int b = 378551;
		unsigned int a = 63689;
		unsigned int hash = 0;
		unsigned int i = 0;
		for (i = 0; i < length; ++data, ++i) {
			hash = hash * a + (*data);
			a = a * b;
		}
		return hash;
	}

	unsigned int Hashing::JSHash(const char * data, unsigned int length) {
		unsigned int hash = 1315423911;
		unsigned int i = 0;
		for (i = 0; i < length; ++data, ++i) {
			hash ^= ((hash << 5) + (*data) + (hash >> 2));
		}
		return hash;
	}

	unsigned int Hashing::PJWHash(const char * data, unsigned int length) {
		const unsigned int BitsInUnsignedInt = (unsigned int)(sizeof(unsigned int) * 8);
		const unsigned int ThreeQuarters = (unsigned int)((BitsInUnsignedInt * 3) / 4);
		const unsigned int OneEighth = (unsigned int)(BitsInUnsignedInt / 8);
		const unsigned int HighBits = (unsigned int)(0xFFFFFFFF) << (BitsInUnsignedInt - OneEighth);
		unsigned int hash = 0;
		unsigned int test = 0;
		unsigned int i = 0;
		for (i = 0; i < length; ++data, ++i) {
			hash = (hash << OneEighth) + (*data);

			if ((test = hash & HighBits) != 0) {
				hash = ((hash ^ (test >> ThreeQuarters)) & (~HighBits));
			}
		}
		return hash;
	}

	void HashTable::AddEntry(unsigned int entry) {
		/*
		bool notDone = true;
		int largerEntry = ;

		for (int i = m_capacity; i != largerEntry; i--) {
			m_table[i] = m_table[i + 1];
		}

		if (m_table.capacity() > 2) {
			for (int i = 0; notDone; i++) {
				if (m_table[i] > entry) {
					largerEntry = i;
					notDone = false;
				}
			}
		}
		m_table[largerEntry - 1] = entry;
		m_capacity += 1;
*/

		m_table.push_back(entry);

		for (auto iter = m_table.begin(); iter != m_table.end(); iter++) {
			if (iter > iter + 1) {
				auto temp = iter;
				auto tempx = iter + 1;
				iter = tempx;
				iter + 1 = temp;
			}
		}
	}
}