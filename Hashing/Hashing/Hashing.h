#pragma once
#include <functional>
#include <vector>

namespace Hashing {

	typedef std::function<unsigned int(const char *, unsigned int)> HashFunc;

	unsigned int badHash(const char* data, unsigned int length);
	unsigned int RSHash(const char* data, unsigned int length);
	unsigned int JSHash(const char* data, unsigned int length);
	unsigned int PJWHash(const char* data, unsigned int length);

	// HashTable that keeps track of Hashed Resources.
	/*template <class T>
	class HashTable {
		struct TableData {};
		HashTable(unsigned int size);

		unsigned int m_TableCapacity;
		std::vector<unsigned int> m_Table;
	};
	*/

	class HashTable {
		public:
		HashTable() { m_capacity = 0; }
		void AddEntry(unsigned int entry);
		std::vector<unsigned int> m_table;
		int m_capacity;
	};
};

// Hashing::HashTable<T> NameOfTable;
// Hashing::AHashTechnique(NameOfTable, Entry);
// Hashing::GetEntry(unsigned int entry);