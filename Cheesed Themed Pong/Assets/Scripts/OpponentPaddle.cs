﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentPaddle : MonoBehaviour {

	GameObject ball;

	// Use this for initialization
	void Start () {
		ball = GameObject.FindGameObjectWithTag("Ball");
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = new Vector3(this.transform.position.x, ball.transform.position.y, this.transform.position.z);
	}
}
