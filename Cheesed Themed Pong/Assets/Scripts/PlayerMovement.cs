﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float moveSpeed = 10.0f;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

		if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer) {

			if (Input.GetAxis("Vertical") != 0.0f) {
				this.transform.position = new Vector3(this.transform.position.x,
														this.transform.position.y + Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime,
														this.transform.position.z);
			}
		}
		if (Application.platform == RuntimePlatform.Android) {
			this.transform.position = new Vector3(this.transform.position.x,
													Input.mousePosition.y,
													this.transform.position.z);
		}

	}
}
