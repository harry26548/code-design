﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour {

	public Vector3 velocity;
	public Vector3 acceleration = new Vector3(0, 0);
	public float AddedAcceleration = 2.0f;
	public float RandomRange = 10.0f;

	// Use this for initialization
	void Start() {
		velocity = new Vector3(Random.Range(-RandomRange, RandomRange), Random.Range(-RandomRange, RandomRange));
	}

	// Update is called once per frame
	void Update() {
		velocity += acceleration * Time.deltaTime;
		this.transform.position += velocity * Time.deltaTime;
		acceleration = new Vector3(0, 0);
	}

	private void OnCollisionEnter(Collision collision) {
		if(collision.gameObject.tag == "UpDownWall") {
			velocity = new Vector3(velocity.x, -velocity.y);
		}
		if (collision.gameObject.tag == "LeftRightWall") {
			velocity = new Vector3(-velocity.x, velocity.y);
		}
		if (collision.gameObject.tag == "Paddle") {
			velocity = new Vector3(-velocity.x, velocity.y);
			acceleration += velocity;
			acceleration.Normalize();
			acceleration *= AddedAcceleration;
		}
	}
}