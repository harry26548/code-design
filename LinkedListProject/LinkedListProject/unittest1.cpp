#include "CppUnitTest.h"
#include "LinkedList.h"
#include <exception>
#include <iostream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace LinkedListProject
{
	TEST_CLASS(LinkedLists) {
public:

	// Passed
	TEST_METHOD(add_to_list_end) {
		LinkedList<int> list;

		list.pushBack(10);
		Assert::AreEqual(list.last(), 10);

		list.pushBack(12);
		Assert::AreEqual(list.last(), 12);

		list.pushBack(13);
		Assert::AreEqual(list.last(), 13);
		//Assert::Fail(L"Test not implemented");
	}

	// Passed
	TEST_METHOD(add_to_list_begin) {
		LinkedList<int> list;

		list.pushFront(13);
		Assert::AreEqual(list.first(), 13);

		list.pushFront(12);
		Assert::AreEqual(list.first(), 12);
	}

	TEST_METHOD(insert_to_list_middle) {
		LinkedList<int> list;
		list.pushFront(20); 
		list.insertAtInd(list.count() / 2, 12);
		list.insertAtInd(list.count() / 2, 13);
		list.insertAtInd(list.count() / 2, 14);
		list.insertAtInd(list.count() / 2, 15);
		list.insertAtInd(list.count() / 2, 16);
		Assert::AreEqual(list.last(), 20);
		// TODO: Your test code here
		//Assert::Fail(L"Test not implemented");
	}

	TEST_METHOD(insert_to_list_begin) {
		LinkedList<int> list;
		list.pushFront(20);
		list.insertAtInd(0, 12);
		list.insertAtInd(0, 13);
		list.insertAtInd(0, 14);
		list.insertAtInd(0, 15);
		list.insertAtInd(0, 16);
		Assert::AreEqual(list.first(), 16);

		// TODO: Your test code here
		//Assert::Fail(L"Test not implemented");
	}

	TEST_METHOD(insert_to_list_end) {
		LinkedList<int> list;
		list.pushFront(20);
		list.insertAtInd(list.count(), 12);
		list.insertAtInd(list.count(), 13);
		list.insertAtInd(list.count(), 14);
		list.insertAtInd(list.count(), 15);
		list.insertAtInd(list.count(), 16);
		Assert::AreEqual(list.last(), 16);

		// TODO: Your test code here
		//Assert::Fail(L"Test not implemented");
	}

	TEST_METHOD(remove_first_value_from_list) {
		LinkedList<int> list;
		list.pushFront(12);
		list.pushFront(13);
		list.pushFront(14);
		list.pushFront(15);
		list.pushFront(16);

		list.popFront();

		Assert::AreEqual(list.first(), 12);
	}

	TEST_METHOD(remove_all_of_found_value_from_list) {
		// TODO: Your test code here
		Assert::Fail(L"Test not implemented");
	}

	TEST_METHOD(remove_at_index_from_list) {
		LinkedList<int> list;
		list.pushBack(12);
		list.pushBack(13);
		list.pushBack(14);
		list.pushBack(15);
		list.pushBack(16);

		list.deleteAtInd(2);

		Assert::AreEqual(list.GetValueAtIndex(2), 15);
	}

	TEST_METHOD(try_remove_at_index_from_list_out_of_bounds) {
		LinkedList<int> list;
		list.pushBack(12);
		try {
			list.deleteAtInd(1);
		} catch(std::exception& e) {
			std::cout << e.what() << std::endl;
		}
	}

	TEST_METHOD(remove_range_from_list) {
		// TODO: Your test code here
		Assert::Fail(L"Test not implemented");
	}

	TEST_METHOD(try_remove_range_from_list_out_of_bounds) {
		// TODO: Your test code here
		Assert::Fail(L"Test not implemented");
	}

	TEST_METHOD(sort_list_ascending) {
		LinkedList<int> list;
		list.pushBack(13);
		list.pushBack(12);
		list.pushBack(11);
		list.pushBack(9);
		list.pushBack(10);

		list.sortList();
		Assert::AreEqual(list.GetValueAtIndex(2), 11);
	}

	TEST_METHOD(get_begin_iterator) {
		// TODO: Your test code here
		Assert::Fail(L"Test not implemented");
	}

	TEST_METHOD(get_end_iterator) {
		// TODO: Your test code here
		Assert::Fail(L"Test not implemented");
	}

	TEST_METHOD(iterate_through_list_forward) {
		// TODO: Your test code here
		// This test would also want to be run after modifying the collection
		// to ensure all next and prev pointers are correct
		Assert::Fail(L"Test not implemented");
	}

	// Passed
	TEST_METHOD(count_list_items) {
		LinkedList<int> list;
		list.pushFront(12);
		list.pushFront(13);
		list.pushFront(14);
		list.pushFront(15);
		list.pushFront(16);
		Assert::AreEqual(list.count(), 5);
	}

	// Passed
	TEST_METHOD(get_first_list_item) {
		LinkedList<int> list;
		list.pushFront(13);
		list.pushFront(14);
		list.pushFront(16);
		Assert::AreEqual(list.first(), 16);
	}

	// Passed
	TEST_METHOD(get_last_list_item) {
		LinkedList<int> list;
		list.pushBack(12);
		list.pushBack(14);
		list.pushBack(16);
		Assert::AreEqual(list.last(), 16);
	}
	};
}