#pragma once


template<typename T>
class LinkedList {
public:

	struct Node {
		Node(T _value) : value(_value) {}

		Node  *Next = nullptr;
		Node  *Previous = nullptr;

		T value;
	};

	struct Iterator {
		Iterator() : ptr(nullptr) {}
		Iterator(Node* _ptr) : ptr(_ptr) {}

		Node*	ptr;

		bool operator != (Iterator _LIter) {}

		// dereference
		T operator * () {}

		// equality
		bool operator== (const Node &rhs) {}

		// increment operators
		Iterator& operator++() {}
		Iterator operator++(int) {}

		// decrement operators
		Iterator operator-- () {}
	};


public:

	// Constructor 
	LinkedList() {
		m_first = nullptr;
		m_last = nullptr;
	}

	// Destructor
	~LinkedList() {}

	T last() {
		return m_last->value;
	}

	T first() {
		return m_first->value;
	}

	void pushBack(const T &value) {
		Node *p = new Node(value);

		p->Previous = nullptr;

		if (m_last == nullptr) {
			m_last = p;
			m_last->Next = nullptr;
			m_last->Previous = nullptr;
			m_first = p;
		} else {
			m_last->Next = p;
			p->Previous = m_last;
			m_last = p;
		}
	}

	void popFront() {
		Node *temp = m_first;
		temp->Previous->Next = nullptr;
		m_first = temp->Previous;
		delete temp;
	}

	void pushFront(const T &value) {
		Node *p = new Node(value);

		p->Next = nullptr;

		if (m_first == nullptr) {
			m_first = p;
			m_first->Next = nullptr;
			m_first->Previous = nullptr;
			m_last = p;
		} else {
			m_first->Previous = p;
			p->Next = m_first;
			m_first = p;
		}
	}

	T GetValueAtIndex(int pos) {
		int count = 0;
		Node *current = m_first;
		while (count != pos && current != nullptr) {
			current = current->Next;
			count++;
		}
		if (current != nullptr) {
			return current->value;
		}
		return NULL;
	}

	void insertAtInd(int pos, const T &value) {
		Node *pre = new Node(0);
		Node *cur = new Node(0);
		Node *temp = new Node(0);

		cur = m_first;

		for (int i = 1; i < pos; i++) {
			pre = cur;
			cur = cur->Next;
		}
		temp->value = value;
		pre->Next = temp;
		temp->Next = cur;
		if (pos == 0) {
			m_first = temp;
		}

		if (pos == this->count()) {
			m_last = temp;
		}
	}

	void deleteAtInd(int pos) {
		Node *current = nullptr;
		Node *previous = nullptr;
		current = m_first;
		for (int i = 1; i < pos; i++) {
			previous = current;
			current = current->Next;
		}
		previous->Next = current->Next;
	}

	int count() {
		Node *currentptr = m_first;
		int count = 0;
		while (currentptr != nullptr) {
			count++;
			currentptr = currentptr->Next;

		}
		return count;
	}

	void sortList() {
		Node* temphead = m_first;
		int tempvalue;
		int counter = 0;
		while (temphead) {
			temphead = temphead->Next;
			counter++;
		}

		temphead = m_first;

		for (int i = 0; i < counter; i++) {
			while (temphead->Next) {
				if (temphead->value > temphead->Next->value) {
					tempvalue = temphead->value;
					temphead->value = temphead->Next->value;
					temphead->Next->value = tempvalue;

					temphead = temphead->Next;
				} else {
					temphead = temphead->Next;
				}
				
			}
			temphead = m_first;
		}
	}

protected:

	Node  *m_first;
	Node  *m_last;
private:
};